package com.gitlab.survivalRaiz.clans.clans;

import com.gitlab.survivalRaiz.core.db.DBManager;
import org.bukkit.Location;

import java.util.UUID;

/**
 * Handles all clan related tasks
 */
public class ClanManager {

    private final DBManager dbManager;

    public ClanManager(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    /**
     * Tells if a clan with that name already exists
     *
     * @param name The name to be tested
     * @return true if a clan with that name already exists
     */
    public boolean isClan(String name) {
        return dbManager.getClansByName(name) == 1;
    }


    /**
     * Tells if the player is the owner of it's clan
     *
     * @param player The player to be tested
     * @return true if the player is owner of it's clan, false otherwise or if he has no clan.
     */
    public boolean isOwner(UUID player) {
        final String[] data = dbManager.hasClan(player);

        if (data == null)
            return false;

        return data[1].equals(Rank.OWNER.name());
    }

    /**
     * Creates a new clan
     *
     * @param owner The clan owner
     * @param name  The clan name
     * @param tag   The clan tag
     */
    public void createClan(UUID owner, String name, String tag) {
        dbManager.createClan(name, tag);

        dbManager.addToClan(owner, name, Rank.OWNER.name());
    }

    /**
     * Gets the player clan's name
     *
     * @param player The player to be tested
     * @return The player's clan name, null if none
     */
    public String getPlayerClan(UUID player) {
        final String[] data = dbManager.hasClan(player);

        return (data == null || data.length == 0) ? null : data[0];
    }

    /**
     * Updates a clan's base.
     * Requires permission validation before used.
     *
     * @param clan  The clan whose base is to be updated
     * @param x     The x of the base
     * @param y     The y of the base
     * @param z     The z of the base
     * @param world The world name
     */
    public void updateClanBase(String clan, int x, int y, int z, String world) {
        //todo: validate worlds that should not have bases (like lobbies and arenas)
        dbManager.updateClanBase(clan, x, y, z, world);
    }

    /**
     * Deletes a clan given it's name
     *
     * @param name The clan's name
     */
    public void deleteClan(String name) {
        dbManager.deleteClan(name);
    }

    /**
     * accepts the invitation sent from the clan to the player
     *
     * @param player The invited player
     * @param clan   The clan who invited him
     */
    public void accept(UUID player, String clan) {
        dbManager.addToClan(player, clan, Rank.RECRUIT.name());
    }
}
