package com.gitlab.survivalRaiz.clans.commands;

import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.clans.clans.Rank;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.Bukkit;

import java.util.*;

/**
 * Handles clan invites
 */
public class RecruitManager {

    private final List<Request> requests = new ArrayList<>();

    private final Clans plugin;

    public RecruitManager(Clans plugin) {
        this.plugin = plugin;
    }

    /**
     * Sends the player an invite. Both need to be online.
     * The invite can be any specified in type
     *
     * @param type      The type of the invitation (clan membership, aly or rival)
     * @param recruiter The player who sends the invite
     * @param player    The player who recieves the invite
     * @return 0 if successful, -1 if the recruiter has no clan, -2 if they have no permissions,
     * -3 if an invite already exists, -4 if the receiver is not te owner where they should be (aly/rival requests)
     */
    public int sendInvitation(UUID recruiter, UUID player, RequestType type) {
        final String clan = plugin.getClanManager().getPlayerClan(recruiter);

        if (clan == null)
            return -1;

        if (Rank.valueOf(plugin.getCore().getDbManager().getClanRank(recruiter)).ordinal() < Rank.MODERATOR.ordinal())
            return -2;

        final Request r = new Request(type, recruiter, player);

        if (contains(r))
            return -3;

        if ((type == RequestType.ALY || type == RequestType.RIVAL) &&
                !plugin.getClanManager().isOwner(player))
            return -4;

        requests.add(r);

        final Message msgType = type == RequestType.INVITE ?
                Message.CLAN_INVITATION :
                type == RequestType.RIVAL ?
                        Message.CLAN_INV_RIV :
                        Message.CLAN_INV_ALY;
        plugin.getCore().getMsgHandler().message(Bukkit.getPlayer(player), msgType,
                s -> s.replaceAll("%clan%", clan),
                s -> s.replaceAll("%code%", String.valueOf(indexOf(r))),
                s -> s.replaceAll("%recruiter%", Bukkit.getPlayer(recruiter).getName()));

        return 0;
    }

    /**
     * Removes the invitation from the map
     *
     * @param player the invited player who answered the invitation
     * @param index  the code of the invitation
     * @return The recruiter's clan name
     * @throws IndexOutOfBoundsException when the index provided is not valid
     */
    public Request answer(UUID player, int index) throws IndexOutOfBoundsException {
        final List<Request> playerInvitations = get(player);
        playerInvitations.forEach(i -> System.out.println(i.getRecruiter()));
        final Request r = playerInvitations.get(index);

        this.requests.remove(r);

//        return plugin.getClanManager().getPlayerClan(r.getRecruiter());
        return r;
    }

    private boolean contains(Request r) {
        for (Request request : requests)
            if (request.equals(r))
                return true;

        return false;
    }

    /**
     * Returns the index of a requst sent to the player, so they can answer a desired request.
     *
     * @param r The request to be found
     * @return The index if found, else -1
     */
    public int indexOf(Request r) {
        final List<Request> requests = get(r.getPlayer());

        for (int i = 0; i < requests.size(); i++)
            if (requests.get(i).equals(r))
                return i;

        return -1;
    }

    private List<Request> get(UUID player) {
        final List<Request> requests = new ArrayList<>();

        for (Request request : this.requests)
            if (request.getPlayer().equals(player))
                requests.add(request);

        return requests;
    }
}

final class Request {
    private final RequestType type;
    private final UUID recruiter;
    private final UUID player;

    Request(RequestType type, UUID recruiter, UUID player) {
        this.type = type;
        this.recruiter = recruiter;
        this.player = player;
    }

    public UUID getPlayer() {
        return player;
    }

    public UUID getRecruiter() {
        return recruiter;
    }

    public RequestType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Request)) return false;

        final Request request = (Request) o;

        return recruiter.equals(request.recruiter) &&
                player.equals(request.player) &&
                type == request.type;
    }
}

enum RequestType {
    INVITE, RIVAL, ALY
}
