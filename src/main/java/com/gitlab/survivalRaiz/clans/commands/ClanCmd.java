package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class ClanCmd extends ConfigCommand {

    private final Clans plugin;

    public ClanCmd(Clans plugin) {
        super("clan", "cria um clan", "/clan <nome> <tag>", new ArrayList<>(), "clan");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        if (strings.length < 2)
           throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());

        if (strings[1].length() != 3)
            throw new SRCommandException(commandSender, Message.INVALID_CLAN_TAG, getUsage(), plugin.getCore());

        if (plugin.getClanManager().isClan(strings[0]))
            throw new SRCommandException(commandSender, Message.CLAN_ALREADY_EXISTS, plugin.getCore());

        // TODO: 1/4/22 Check if player already has clan

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        final UUID owner = ((Player) commandSender).getUniqueId();

        plugin.getClanManager().createClan(owner, strings[0], strings[1]);

        // TODO: 1/4/22 Send message on success
    }
}
