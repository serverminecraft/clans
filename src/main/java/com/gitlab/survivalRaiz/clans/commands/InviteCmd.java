package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class InviteCmd extends ConfigCommand {

    private final Clans plugin;

    public InviteCmd(Clans clans) {
        super("convidar", "convida um jogador para o clan, ou para fazer o seu clan aliado/rival",
                "/convidar <jogador> [tipo]", new ArrayList<>(), "convidar");
        this.plugin = clans;
    }

    /**
     * 0 -> membro
     * 1 -> aliado
     * 2 -> rival
     *
     * @param commandSender
     * @param s
     * @param strings
     * @return
     * @throws CommandException
     */
    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (strings.length < 1)
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        if (Bukkit.getPlayer(strings[0]) == null)
            throw new SRCommandException(p, Message.PLAYER_NOT_FOUND, plugin.getCore());

        final String name = plugin.getCore().getDbManager().hasClan(p.getUniqueId())[0];
        if (plugin.getCore().getDbManager().getNMembers(name) > 5) // TODO: 12/25/21 move to config, make depend on level
            throw new SRCommandException(p, Message.CLAN_IS_FULL, plugin.getCore());

        if (strings.length >= 2)
            switch (strings[1]) {
                case "aliado":
                    return 1;
                case "rival":
                    return 2;
                case "membro":
                    return 0;
                default:
                    throw new SRCommandException(p, Message.SYNTAX, plugin.getCore());
            }

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);

        final UUID recruiter = ((Player) commandSender).getUniqueId();
        final UUID player = Bukkit.getPlayer(strings[0]).getUniqueId();

        final RequestType requestType = syntax == 0 ?
                RequestType.INVITE : syntax == 1 ?
                RequestType.ALY :
                RequestType.RIVAL;

        final int status = plugin.getRecruitManager().sendInvitation(recruiter, player, requestType);

        switch (status) {
            case -2:
            case -4:
                throw new SRCommandException(commandSender, Message.NO_PERMISSIONS, plugin.getCore());
            case -3:
                throw new SRCommandException(commandSender, Message.REQUEST_ALREADY_EXISTS, plugin.getCore());
            case 0:
                plugin.getCore().getMsgHandler().message(commandSender, Message.REQUEST_SENT,
                        s1 -> s1.replaceAll("%type%", requestType.name()),
                        s1 -> s1.replaceAll("%user%", strings[0]));
                return;
            default:
                throw new SRCommandException(commandSender, Message.PLAYER_NEEDS_CLAN, plugin.getCore());
        }
    }
}
