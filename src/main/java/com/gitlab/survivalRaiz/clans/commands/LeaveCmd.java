package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.swing.*;
import java.util.ArrayList;
import java.util.UUID;

public class LeaveCmd extends ConfigCommand {

    private final Clans plugin;

    public LeaveCmd(Clans plugin) {
        super("sair", "sai do clan atual",
                "/sair", new ArrayList<>(), "sair");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
                throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (plugin.getCore().getDbManager().hasClan(p.getUniqueId()) == null)
            throw new SRCommandException(p, Message.PLAYER_NEEDS_CLAN, plugin.getCore());

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        plugin.getCore().getDbManager().removeMember(((Player) commandSender).getUniqueId());
    }
}
