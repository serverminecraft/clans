package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class RivalsCmd extends ConfigCommand {

    private final Clans plugin;

    public RivalsCmd(Clans plugin) {
        super("rivais", "Mostra os rivais do clan", "/rivais", new ArrayList<>(), "rivais");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        final String clanName = plugin.getCore().getDbManager().hasClan(((Player) commandSender).getUniqueId())[0];
        final List<String> rivals = plugin.getCore().getDbManager().getRivals(clanName);

        plugin.getCore().getMsgHandler().message(commandSender, Message.CLAN_RIVALS_HEADER);

        rivals.forEach(c ->
                plugin.getCore().getMsgHandler().message(commandSender, Message.CLAN_LIST_RIVALS,
                        s1 -> s1.replaceAll("%name%", c))
        );
    }
}
