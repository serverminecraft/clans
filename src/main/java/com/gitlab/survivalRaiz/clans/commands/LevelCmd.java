package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class LevelCmd extends ConfigCommand {
    private final Clans plugin;

    public LevelCmd(Clans plugin) {
        super("clan-nivel",
                "mostra o nível do clan. Se não especificado, mostra o nível do clan do jogador",
                "/clan-nivel [clan]", new ArrayList<>(), "clan-nivel");
        this.plugin = plugin;
    }

    /**
     * 0 -> self clan
     * 1 -> clan via args
     *
     * @param commandSender
     * @param s
     * @param strings
     * @return
     * @throws CommandException
     */
    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length == 0) { // No args, show self
            if (!(commandSender instanceof Player))
                throw new SRCommandException(commandSender, Message.PLAYER_HAS_NO_CLAN, plugin.getCore());

            final Player p = (Player) commandSender;

            // has no clan
            if (plugin.getCore().getDbManager().hasClan(p.getUniqueId()).length == 0)
                throw new SRCommandException(p, Message.PLAYER_HAS_NO_CLAN, plugin.getCore());

            return 0;
        }

        // show clan rank
        final String clanName = strings[0];

        //clan does not exist
        if (plugin.getCore().getDbManager().getClansByName(clanName) == 0)
            throw new SRCommandException(commandSender, Message.NONEXISTENT_CLAN, plugin.getCore());

        return 1;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        switch (validate(commandSender, s, strings)) {
            case 0:
                plugin.getCore().getMsgHandler().message(
                        commandSender, Message.CLAN_RANK, s1 -> s1.replaceAll(
                                "_rank_",
                                String.valueOf(plugin.getCore().getDbManager()
                                        .hasClan(((Player) commandSender).getUniqueId())[0]))
                );
                break;
            case 1:
                plugin.getCore().getMsgHandler().message(
                        commandSender, Message.CLAN_RANK, s1 -> s1.replaceAll(
                                "_rank_",
                                String.valueOf(plugin.getCore().getDbManager().getClanLevel(strings[0])))
                );
                break;
            default:
                throw new SRCommandException(commandSender, Message.NONEXISTENT_CLAN, plugin.getCore());
        }
    }
}
