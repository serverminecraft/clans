package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class TopCmd extends ConfigCommand {

    private final Clans plugin;

    public TopCmd(Clans plugin) {
        super("clan-top", "Mostra os clans com mais pontos", "/clan-top [n]",
                new ArrayList<>(), "clan-top");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {

        if (strings.length > 0)
            try {
                return Integer.parseInt(strings[0]);
            } catch (NumberFormatException e) {
                throw new SRCommandException(commandSender, Message.SYNTAX, plugin.getCore());
            }

        return 10;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int topn = validate(commandSender, s, strings);
        final List<List<Object>> top = plugin.getCore().getDbManager().getTopClans(topn);

        plugin.getCore().getMsgHandler().message(commandSender, Message.CLAN_TOP_HEADER);

        top.forEach(c ->
                plugin.getCore().getMsgHandler().message(commandSender, Message.CLAN_TOP_LINE,
                        s1 -> s1
                                .replaceAll("%name%", (String) c.get(0))
                                .replaceAll("%level%", String.valueOf(c.get(1))))
        );
    }
}
