package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.clans.clans.Rank;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class RankCmd extends ConfigCommand {

    private final Clans plugin;

    public RankCmd(Clans plugin) {
        super("rank", "altera o rank de um jogador",
                "/rank <jogador> [+/-]", new ArrayList<>(), "rank");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length < 1)
            throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());

        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (!Bukkit.getOfflinePlayer(strings[0]).hasPlayedBefore())
            throw new SRCommandException(p, Message.PLAYER_NOT_FOUND, plugin.getCore());

        final UUID op = Bukkit.getOfflinePlayer(strings[0]).getUniqueId();
        final String opClan = plugin.getClanManager().getPlayerClan(op);
        final String clan = plugin.getClanManager().getPlayerClan(p.getUniqueId());

        if (opClan == null)
            throw new SRCommandException(p, Message.PLAYER_NOT_FOUND, plugin.getCore());

        if (!opClan.equals(clan))
            throw new SRCommandException(p, Message.PLAYER_NOT_FOUND, plugin.getCore());

        try {
            if (strings[1].equals("+") || strings[1].equals("-"))
                throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());
            return 1;
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }

        // 0 -> mostra o rnak
        // 1 -> incrementa/decrementa o rank
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);
        final String clanRank = plugin.getCore().getDbManager().getClanRank(
                Bukkit.getOfflinePlayer(strings[0]).getUniqueId());

        if (syntax == 0)
            if (clanRank == null)
                plugin.getCore().getMsgHandler().message(commandSender,
                        Message.PLAYER_HAS_NO_CLAN);
            else
                plugin.getCore().getMsgHandler().message(commandSender,
                        Message.PLAYER_CLAN_RANK,
                        str -> str.replaceAll("%rank%", clanRank));

        final int modifier = strings[1].equals("+") ? 1 : -1;
        final int ordinal = Rank.valueOf(clanRank).ordinal();

        final int newRank = Rank.values().length % ordinal + modifier;

        plugin.getCore().getDbManager().setClanRank(
                Bukkit.getOfflinePlayer(strings[0]).getUniqueId(),
                Rank.values()[newRank].name());
    }
}
