package com.gitlab.survivalRaiz.clans;

import api.skwead.commands.CommandManager;
import api.skwead.commands.ConfigCommand;
import com.gitlab.survivalRaiz.clans.clans.ClanManager;
import com.gitlab.survivalRaiz.clans.commands.*;
import com.gitlab.survivalRaiz.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Clans extends JavaPlugin {

    private Core core;
    private ClanManager clanManager;
    private RecruitManager recruitManager;

    @Override
    public void onEnable() {
        setupCore();

        recruitManager = new RecruitManager(this);
        clanManager = new ClanManager(core.getDbManager());

        setupCommands();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private boolean setupCore() {
        if (Bukkit.getPluginManager().getPlugin("Core") == null)
            return false;

        this.core = (Core) Bukkit.getPluginManager().getPlugin("Core");
        return true;
    }

    private void setupCommands() {
        final Set<ConfigCommand> cmds = new HashSet<>();
        final CommandManager cm = new CommandManager(this);

        cmds.add(new ClanCmd(this));
        cmds.add(new DelClanCmd(this));
        cmds.add(new RankCmd(this));
        cmds.add(new SetBaseCmd(this));
        cmds.add(new InviteCmd(this));
        cmds.add(new AcceptCmd(this));
        cmds.add(new DenyCmd(this));
        cmds.add(new LeaveCmd(this));
        cmds.add(new LevelCmd(this));
        cmds.add(new TopCmd(this));
        cmds.add(new AliesCmd(this));
        cmds.add(new RivalsCmd(this));

        try {
            cm.getConfig().generateFrom(cmds);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public ClanManager getClanManager() {
        return clanManager;
    }

    public Core getCore() {
        return core;
    }

    public RecruitManager getRecruitManager() {
        return recruitManager;
    }
}
